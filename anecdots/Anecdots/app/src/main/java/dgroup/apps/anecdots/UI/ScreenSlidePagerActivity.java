package dgroup.apps.anecdots.UI;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import dgroup.apps.anecdots.AnecdotsApplication;
import dgroup.apps.anecdots.R;
import dgroup.apps.anecdots.UI.dialogfactory.DialogCreator;
import dgroup.apps.anecdots.core.DBHelper;
import dgroup.apps.anecdots.core.Key;


public class ScreenSlidePagerActivity extends Activity {

    private int numPages;

    private ViewPager mPager;

    private ScreenSlidePagerAdapter mPagerAdapter;

    private static final String DEBUG_TAG = "ScreenSlidePagerActivity";

    private TextView progressState;

    private SeekBar pageControl = null;

    private Button addToFavorite;

    private Button prev;

    private Button next;

    private Button sendMail;

    private Button trash;

    private String currentCategory;

    private Cursor cursor;

    private int currentPosition;

    private boolean [] selectedItems;

    private SparseBooleanArray changes;

    private HashMap<Key,Boolean> randomChanges;

    private boolean isFavorite = false;

    private boolean isRandomOrSearch = false;

    private Dialog checkDialog;

    private DialogCreator dialogCreator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_slide);
        if(getActionBar()!=null)
            getActionBar().setDisplayHomeAsUpEnabled(true);

        initElements(savedInstanceState);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(cursor!=null && !currentCategory.equals(getString(R.string.search_result)))
            cursor.close();
        AnecdotsApplication.getSharedPreferences().edit().putInt(currentCategory, currentPosition).apply();
    }


    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(!isFavorite) {
            outState.putBooleanArray("selectedItems", selectedItems);
        }
        if(isRandomOrSearch){
            outState.putBoolean("israndom",true);
            Iterator it = randomChanges.entrySet().iterator();
            ArrayList<Integer> numbers = new ArrayList<>();
            ArrayList<CharSequence> categories = new ArrayList<>();

            outState.putInt("mapsize",randomChanges.size());
            outState.putBooleanArray("mapvalues",toPrimitiveArray(randomChanges.values()));
            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry)it.next();
                numbers.add(((Key)pairs.getKey()).getNumber());
                categories.add(((Key)pairs.getKey()).getCategory());
            }
            outState.putIntegerArrayList("numbers",numbers);
            outState.putCharSequenceArrayList("categories",categories);
        }
        if(checkDialog!=null){
            outState.putBoolean("isSearchDialogVisible",checkDialog.isShowing());
        }
    }


    private boolean[] toPrimitiveArray(final Collection<Boolean> booleanCollection) {
        final boolean[] primitives = new boolean[booleanCollection.size()];
        int index = 0;
        for (Boolean object : booleanCollection) {
            primitives[index++] = object;
        }
        return primitives;
    }

    private void initElements(Bundle savedInstanceState){

        changes = new SparseBooleanArray();

        progressState = (TextView) findViewById(R.id.progress);

        Intent data = getIntent();

        mPager = (ViewPager) findViewById(R.id.pager);

        currentCategory = data.getStringExtra("title");

        dialogCreator = new DialogCreator(this);

        switch (currentCategory){
            case "Избранное":
                cursor  = DBHelper.instance().getFavorites();
                isFavorite = true;
                break;
            case "Случайное":
                cursor  = DBHelper.instance().getRandom();
                randomChanges = new HashMap<>();
                isRandomOrSearch = true;
                break;
            case "Результаты поиска":
                randomChanges = new HashMap<>();
                cursor = DBHelper.instance().getCursor();
                isRandomOrSearch = true;
                break;
            default:
                cursor = DBHelper.instance().getAllStory(currentCategory);
                break;
        }

        numPages = cursor.getCount();

        selectedItems = new boolean[numPages];

        if(savedInstanceState==null) {
            int i = 0;
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                selectedItems[i] = cursor.getInt(cursor.getColumnIndex(DBHelper.ISFAVORITE)) > 0;
                i++;
            }
        }else{
            selectedItems = savedInstanceState.getBooleanArray("selectedItems");
            if(savedInstanceState.getBoolean("israndom",false)){
                int size = savedInstanceState.getInt("mapsize",0);

                ArrayList<Integer> numbers = savedInstanceState.getIntegerArrayList("numbers");
                ArrayList<CharSequence> categories = savedInstanceState.getCharSequenceArrayList("categories");
                boolean [] values = savedInstanceState.getBooleanArray("mapvalues");

                for(int i=0; i<size; i++){
                    randomChanges.put(new Key(categories.get(i).toString(),numbers.get(i)),values[i]);
                }
            }
            if(savedInstanceState.getBoolean("isSearchDialogVisible")){
                checkDialog = dialogCreator.getDialog(DialogCreator.MODE_CHECK);
                checkDialog.show();
            }
        }

        if(getActionBar()!=null)
            getActionBar().setTitle(currentCategory);

        pageControl = (SeekBar)findViewById(R.id.seekBar);
        pageControl.setMax(numPages-1);

        pageControl.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChanged = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
                progressChanged = progress+1;
                currentPosition = progress;
                progressState.setText(progressChanged+"/"+numPages);
            }

            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                mPager.setCurrentItem(seekBar.getProgress());
                currentPosition = seekBar.getProgress();
            }
        });

        int startPosition = 0;
        if(AnecdotsApplication.getSharedPreferences().getInt(currentCategory,0)!=0 && !currentCategory.equals(getString(R.string.random)) && !currentCategory.equals(getString(R.string.search_result))){
            startPosition = AnecdotsApplication.getSharedPreferences().getInt(currentCategory, 0);
            if(startPosition == 1 && currentCategory.equals(getString(R.string.favorite))){
                startPosition = 0;
            }
        }

        mPagerAdapter = new ScreenSlidePagerAdapter(getFragmentManager(),cursor);
        mPager.setAdapter(mPagerAdapter);

        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                pageControl.setProgress(position);
                setSelectedFavorite(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        prev = (Button) findViewById(R.id.prev);
        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mPager.getCurrentItem()==0){
                    mPager.setCurrentItem(numPages);
                }else {
                    mPager.setCurrentItem(mPager.getCurrentItem() - 1);
                }
            }
        });
        next = (Button) findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mPager.getCurrentItem()==numPages-1){
                    mPager.setCurrentItem(0);
                }else {
                    mPager.setCurrentItem(mPager.getCurrentItem() + 1);
                }
            }
        });

        sendMail = (Button) findViewById(R.id.sendmail);
        sendMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, ((TextView) (mPager.findViewWithTag("pos" + mPager.getCurrentItem())).findViewById(R.id.text)).getText().toString() + "\nОтправлено из приложения \"Лучшие анекдоты\"");
                    sendIntent.setType("text/plain");
                    startActivity(sendIntent);
                }catch (Exception e){}
            }
        });

        addToFavorite = (Button) findViewById(R.id.addtofav);
        addToFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(addToFavorite.isSelected()) {
                    addToFavorite.setSelected(false);
                    selectedItems[currentPosition] = false;
                    changes.put(currentPosition,false);
                    if(isRandomOrSearch){
                        cursor.moveToPosition(currentPosition);
                        randomChanges.put(new Key(cursor.getString(cursor.getColumnIndex(DBHelper.CATEGORY)),cursor.getInt(cursor.getColumnIndex(DBHelper.NUMBER))),false);
                    }
                }else{
                    addToFavorite.setSelected(true);
                    selectedItems[currentPosition] = true;
                    changes.put(currentPosition,true);
                    if(isRandomOrSearch){
                        cursor.moveToPosition(currentPosition);
                        randomChanges.put(new Key(cursor.getString(cursor.getColumnIndex(DBHelper.CATEGORY)),cursor.getInt(cursor.getColumnIndex(DBHelper.NUMBER))),true);
                    }
                }
            }
        });

        trash = (Button) findViewById(R.id.trash);
        trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkDialog = dialogCreator.getDialog(DialogCreator.MODE_CHECK);
                checkDialog.show();
            }
        });

        if(!isFavorite){
            addToFavorite.setSelected(selectedItems[currentPosition]);
            trash.setVisibility(View.INVISIBLE);
        }else{
            addToFavorite.setVisibility(View.INVISIBLE);
            trash.setVisibility(View.VISIBLE);
        }
        if(startPosition!=0)
            progressState.setText(startPosition+"/"+numPages);
        else
            progressState.setText(1 + startPosition + "/" + numPages);
        pageControl.setProgress(startPosition);
        mPager.setCurrentItem(startPosition);

        prepareVisibility();
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        private Cursor cursor;

        public ScreenSlidePagerAdapter(FragmentManager fm, Cursor cursor){
            super(fm);
            this.cursor = cursor;
        }

        @Override
        public Fragment getItem(int position) {
            Log.i(DEBUG_TAG, "get item " + position);
            if (cursor == null)
                return null;

            cursor.moveToPosition(position);
            Bundle args = new Bundle();
            args.putString("content",cursor.getString(cursor.getColumnIndex(DBHelper.CONTENT)));
            args.putInt("pos",position);
            ScreenSlidePageFragment screenSlidePageFragment = new ScreenSlidePageFragment();
            screenSlidePageFragment.setArguments(args);
            return screenSlidePageFragment;
        }

        @Override
        public int getCount() {
            if (cursor == null)
                return 0;
            else
                return cursor.getCount();
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        public void swapCursor(Cursor c) {
            if (cursor == c) {
                return;
            }

            this.cursor = c;
            notifyDataSetChanged();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(!isFavorite) {
            saveSelected();
        }
        if(cursor!=null && currentCategory.equals(getString(R.string.search_result))){
            cursor.close();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                if(!isFavorite) {
                    saveSelected();
                }
                if(cursor!=null && currentCategory.equals(getString(R.string.search_result))){
                    cursor.close();
                }
                return true;
            case R.id.action_settings:
                dialogCreator.getDialog(DialogCreator.MODE_MENU).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setSelectedFavorite(int position){
        if(addToFavorite.getVisibility() == View.VISIBLE)
            addToFavorite.setSelected(selectedItems[position]);
    }


    private void saveSelected(){
        try {
            if(isRandomOrSearch){
                DBHelper.instance().updateFavorites(randomChanges);
            }else {
                DBHelper.instance().updateFavorites(changes, currentCategory);
            }
            Log.i("save","save to favorite complite");
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private void prepareVisibility(){
        if(cursor!=null && cursor.getCount()==0){
            pageControl.setVisibility(View.INVISIBLE);
            prev.setVisibility(View.INVISIBLE);
            next.setVisibility(View.INVISIBLE);
            progressState.setVisibility(View.INVISIBLE);
            sendMail.setVisibility(View.INVISIBLE);
            addToFavorite.setVisibility(View.INVISIBLE);
            trash.setVisibility(View.INVISIBLE);

        }else{
            TextView tv = (TextView) findViewById(R.id.empty);
            tv.setVisibility(View.INVISIBLE);
        }
    }

    public void prepare(){
        if (cursor.getCount() == currentPosition)
            cursor.moveToPosition(currentPosition - 1);
        else
            cursor.moveToPosition(currentPosition);
        DBHelper.instance().removeFavorite(cursor.getString(cursor.getColumnIndex(DBHelper.CATEGORY)), cursor.getInt(cursor.getColumnIndex(DBHelper.NUMBER)));
        Log.i("swap", "cat=" + cursor.getString(cursor.getColumnIndex(DBHelper.CATEGORY)) + " num=" + cursor.getInt(cursor.getColumnIndex(DBHelper.NUMBER)));
        cursor.close();
        cursor = DBHelper.instance().getFavorites();
        numPages = cursor.getCount();
        mPagerAdapter.swapCursor(cursor);
        if(cursor!=null && cursor.getCount()==0) {
            prepareVisibility();
            TextView tv = (TextView) findViewById(R.id.empty);
            tv.setVisibility(View.VISIBLE);
            AnecdotsApplication.getSharedPreferences().edit().putInt(currentCategory,0).apply();
        }
        else {
            pageControl.setMax(numPages - 1);
            currentPosition++;
            progressState.setText(currentPosition + "/" + numPages);
        }

    }

    public void refreshFragmentAdapter(){
        mPagerAdapter.notifyDataSetChanged();
    }

}