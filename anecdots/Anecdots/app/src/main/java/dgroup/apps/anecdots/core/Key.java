package dgroup.apps.anecdots.core;


public class Key {

    private final String category;
    private final int number;

    public Key(String category, int number) {
        this.category = category;
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Key)) return false;
        Key key = (Key) o;
        return category.equals(key.category) && number == key.number;
    }

    @Override
    public int hashCode() {
        int result = number;
        result = 31 * result + category.hashCode();
        return result;
    }

    public String getCategory(){
        return category;
    }

    public int getNumber(){
        return number;
    }

}