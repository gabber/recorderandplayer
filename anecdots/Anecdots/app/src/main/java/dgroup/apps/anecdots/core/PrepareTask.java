package dgroup.apps.anecdots.core;

import android.database.sqlite.SQLiteStatement;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

import dgroup.apps.anecdots.AnecdotsApplication;
import dgroup.apps.anecdots.R;

/**
 * Created by dmitriy on 28.07.14.
 */
public class PrepareTask extends AsyncTask<Void, String, Boolean> {

    private Boolean mResult;
    private String mProgressMessage;
    private IProgressTracker mProgressTracker;


    public PrepareTask() {

    }


    public void setProgressTracker(IProgressTracker progressTracker) {

        mProgressTracker = progressTracker;

        if (mProgressTracker != null) {
            mProgressTracker.onProgress(mProgressMessage);
            if (mResult != null) {
                mProgressTracker.onComplete();
            }
        }
    }


    @Override
    protected void onCancelled() {

        mProgressTracker = null;
    }


    @Override
    protected void onProgressUpdate(String... values) {

        mProgressMessage = values[0];

        if (mProgressTracker != null) {
            mProgressTracker.onProgress(mProgressMessage);
        }
    }


    @Override
    protected void onPostExecute(Boolean result) {

        mResult = result;

        if (mProgressTracker != null) {
            mProgressTracker.onComplete();
        }

        mProgressTracker = null;
    }


    @Override
    protected Boolean doInBackground(Void... arg0) {
        BufferedReader reader = null;
        try {
            String query = "insert into " + DBHelper.TABLE_NAME + " (content,isfavorite,category,number) values ((?),(?),(?),(?));";
            DBHelper.instance().beginTransaction();
            SQLiteStatement insert = DBHelper.instance().getStatement(query);
            ArrayList<Pair<String, Integer>> helper = createHelperList();
            reader = new BufferedReader(new InputStreamReader(AnecdotsApplication.getContext().getResources().openRawResource(R.raw.content)));
            for (int i = 0; i < helper.size(); i++) {
                Log.i("addtobase", "cat=" + helper.get(i).first);
                StringBuilder sb = new StringBuilder();
                String line;

                int count = 0;
                while ((line = reader.readLine()) != null && count < helper.get(i).second) {

                    if (line.isEmpty()) {
                        if (!sb.toString().equals("")) {
                            insert.bindString(1, sb.toString());
                            insert.bindLong(2, 0);
                            insert.bindString(3, helper.get(i).first);
                            insert.bindLong(4, count);
                            insert.executeInsert();
                            insert.clearBindings();
//                            if(count < 3)
//                                Log.i("addtobase", "add anecdot text=" + sb.toString());
                            count++;
                            sb.setLength(0);
                        }
                        continue;
                    }
                    sb.append(line).append("\n");
                    if(count == 0 )
                        Log.i("addtobase", "add anecdot texte=" + sb.toString());

                }
            }
            DBHelper.instance().setTransactionSuccessful();
            DBHelper.instance().endTransaction();
        }catch (Exception e){return false;}finally {
            if(reader!=null) {
                try {
                    reader.close();
                }catch (Exception e){}
            }
        }
        return true;
    }


    private ArrayList<Pair<String,Integer>> createHelperList(){
        ArrayList<Pair<String,Integer>> helper = new ArrayList<>();
        helper.add(new Pair<>("Студенты и школьники", 899));
        helper.add(new Pair<>("Мужские", 900));
        helper.add(new Pair<>("Милицейские", 900));
        helper.add(new Pair<>("Компьютерщики", 900));
        helper.add(new Pair<>("Афоризмы", 900));
        helper.add(new Pair<>("Спорт", 817));
        helper.add(new Pair<>("Новые русские", 856));
        helper.add(new Pair<>("Семья", 899));
        helper.add(new Pair<>("Про евреев", 703));
        helper.add(new Pair<>("Национальности", 797));
        helper.add(new Pair<>("Персонажи", 897));
        helper.add(new Pair<>("Врачебная практика", 902));
        helper.add(new Pair<>("Разные", 900));
        helper.add(new Pair<>("Армия", 900));
        helper.add(new Pair<>("Работа", 900));
        helper.add(new Pair<>("Животные", 639));
        helper.add(new Pair<>("Чёрный юмор", 817));
        helper.add(new Pair<>("Про наркоманов", 246));
        return helper;
    }

}