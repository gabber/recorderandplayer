package dgroup.apps.anecdots.UI.dialogfactory;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import dgroup.apps.anecdots.AnecdotsApplication;
import dgroup.apps.anecdots.R;
import dgroup.apps.anecdots.UI.ScreenSlidePagerActivity;


class MenuDialog extends Dialog implements
        android.view.View.OnClickListener {


    private Button small;
    private Button medium;
    private Button large;
    private Activity activity;


    public MenuDialog(Activity activity) {
        super(activity);
        this.activity = activity;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.menu_dialog);


        small = (Button)findViewById(R.id.smallcheck);
        small.setOnClickListener(this);
        medium = (Button)findViewById(R.id.mediumcheck);
        medium.setOnClickListener(this);
        large = (Button)findViewById(R.id.largecheck);
        large.setOnClickListener(this);
        int curPos = AnecdotsApplication.getSharedPreferences().getInt("selectedfontpos",-1);
        if(curPos ==-1){
            medium.setSelected(true);
        }else{
            switch (curPos){
                case 0:
                    small.setSelected(true);
                    break;
                case 1:
                    medium.setSelected(true);
                    break;
                case 2:
                    large.setSelected(true);
                    break;
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.smallcheck:
                medium.setSelected(false);
                large.setSelected(false);
                small.setSelected(true);
                prepare(0);
                break;
            case R.id.mediumcheck:
                small.setSelected(false);
                large.setSelected(false);
                medium.setSelected(true);
                prepare(1);
                break;
            case R.id.largecheck:
                small.setSelected(false);
                medium.setSelected(false);
                large.setSelected(true);
                prepare(2);
                break;
            default:
                break;
        }

         new Thread(new Runnable() {
             @Override
             public void run() {
                 try {
                    Thread.sleep(100);

                 }catch (Exception e){
                     e.printStackTrace();
                 }
                 activity.runOnUiThread(new Runnable() {
                     @Override
                     public void run() {
                         dismiss();
                     }
                 });

             }
         }).start();
    }

    private void prepare(int pos){
        AnecdotsApplication.getSharedPreferences().edit().putInt("selectedfontpos",pos).apply();
        ((ScreenSlidePagerActivity)activity).refreshFragmentAdapter();
    }
}