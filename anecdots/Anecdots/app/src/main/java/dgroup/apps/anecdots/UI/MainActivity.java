package dgroup.apps.anecdots.UI;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import dgroup.apps.anecdots.AnecdotsApplication;
import dgroup.apps.anecdots.R;
import dgroup.apps.anecdots.UI.dialogfactory.DialogCreator;
import dgroup.apps.anecdots.UI.dialogfactory.SearchDialog;
import dgroup.apps.anecdots.core.AsyncTaskManager;
import dgroup.apps.anecdots.core.DBHelper;
import dgroup.apps.anecdots.core.OnTaskCompleteListener;
import dgroup.apps.anecdots.core.PrepareTask;
import dgroup.apps.anecdots.core.SearchTask;


public class MainActivity extends Activity implements OnTaskCompleteListener {

    private String [] categories;

    private Dialog searchDialog;

    private AsyncTaskManager mAsyncTaskManager;

    private DialogCreator dialogCreator;

    private Dialog prepareDialog;

    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Crashlytics.start(this);

        setContentView(R.layout.activity_main);

        dialogCreator = new DialogCreator(this);

        if(AnecdotsApplication.getSharedPreferences().getBoolean("isparsenow",false)){
            mAsyncTaskManager = new AsyncTaskManager(this, this,false);
        }else{
            mAsyncTaskManager = new AsyncTaskManager(this, this,true);
        }


        mAsyncTaskManager.handleRetainedTask(getLastNonConfigurationInstance());
        listView = (ListView) findViewById(R.id.listView);



        Button favorite = (Button) findViewById(R.id.favorite);
        favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ScreenSlidePagerActivity.class);
                intent.putExtra("title", getResources().getString(R.string.favorite));
                startActivity(intent);
            }
        });

        Button random = (Button) findViewById(R.id.random);
        random.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ScreenSlidePagerActivity.class);
                intent.putExtra("title", getResources().getString(R.string.random));
                startActivity(intent);
            }
        });

        Button search = (Button) findViewById(R.id.search);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAsyncTaskManager = new AsyncTaskManager(MainActivity.this, MainActivity.this,true);
                if(searchDialog == null || !searchDialog.isShowing()) {
                    searchDialog = dialogCreator.getDialog(DialogCreator.MODE_SEARCH);
                    searchDialog.show();
                }
            }
        });
        firstPrepare();
    }


    @Override
    public void onResume(){
        super.onResume();

//        Log.i("ololo"," ololo bool="+AnecdotsApplication.isIsContentPrepare());
//        if(AnecdotsApplication.isIsContentPrepare()){
//
//        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(searchDialog!=null){
            outState.putBoolean("isSearchDialogVisible",searchDialog.isShowing());
            outState.putString("text",((SearchDialog)searchDialog).getText());
        }
    }


    @Override
    protected void onRestoreInstanceState(Bundle outState) {
        super.onRestoreInstanceState(outState);
        if(outState.getBoolean("isSearchDialogVisible",false)){
            searchDialog = dialogCreator.getDialog(DialogCreator.MODE_SEARCH);
            ((SearchDialog)searchDialog).setText(outState.getString("text"));
            searchDialog.show();
        }
    }


    @Override
    public Object onRetainNonConfigurationInstance() {

        return mAsyncTaskManager.retainTask();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
    }


    private String[] getCategories(){
        String [] categories = null;
        Cursor cursor = DBHelper.instance().getAllCategories();
        if(cursor!=null && cursor.getCount()!=0){
            categories = new String[cursor.getCount()];
            int i=0;
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
               categories[i] =cursor.getString(cursor.getColumnIndex(DBHelper.CATEGORY));
                i++;
            }
        }
        if(cursor!=null)
            cursor.close();
        return categories;
    }

    @Override
    public void onTaskComplete(SearchTask task) {
        if (task.isCancelled()) {
            Toast.makeText(MainActivity.this, "вы отменили поиск", Toast.LENGTH_SHORT).show();
        } else {
            try {
                boolean result = task.get();
                if(result){
                    Intent intent = new Intent(MainActivity.this, ScreenSlidePagerActivity.class);
                    intent.putExtra("title", getResources().getString(R.string.search_result));
                    startActivity(intent);
                }else{
                    Toast.makeText(MainActivity.this,"Ничего не найдено",Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onTaskComplete(PrepareTask task) {
        AnecdotsApplication.getSharedPreferences().edit().putBoolean("isparsenow", false).apply();
        createList();
    }

    public void setupTask(String textSearch){
        mAsyncTaskManager.setupTask(new SearchTask(textSearch));
    }

    private void setupTask(){
        mAsyncTaskManager.setupTask(new PrepareTask());
    }

    private void firstPrepare(){
        if(DBHelper.instance().isEmpty() && AnecdotsApplication.getSharedPreferences().getBoolean("isfirststart",true)){
            AnecdotsApplication.getSharedPreferences().edit().putBoolean("isfirststart",false).putBoolean("isparsenow",true).apply();
            mAsyncTaskManager = new AsyncTaskManager(this, this,false);
            setupTask();
        }else{
            createList();
        }

    }

    private void createList(){
        categories = getCategories();
        if(categories!=null) {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, categories);
            listView.setAdapter(adapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(MainActivity.this, ScreenSlidePagerActivity.class);
                    intent.putExtra("title", categories[position]);
                    startActivity(intent);
                }
            });
        }
    }
}
