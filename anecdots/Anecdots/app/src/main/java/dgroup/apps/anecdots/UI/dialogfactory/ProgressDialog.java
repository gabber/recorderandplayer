package dgroup.apps.anecdots.UI.dialogfactory;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.Window;

import dgroup.apps.anecdots.R;

class ProgressDialog extends Dialog{

    public ProgressDialog(Activity activity) {
        super(activity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.action_search_dialog);
    }
}