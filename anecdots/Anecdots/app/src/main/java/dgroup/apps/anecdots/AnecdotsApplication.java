package dgroup.apps.anecdots;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import dgroup.apps.anecdots.core.DBHelper;

public class AnecdotsApplication extends Application {

    private static SharedPreferences appPrefs;

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();

        appPrefs = getSharedPreferences("AnecdotsApplication", MODE_PRIVATE);

        context = getApplicationContext();

        Log.i("ololo","ololo oncreate db instance");
        DBHelper.instance(context);
    }

    public static SharedPreferences getSharedPreferences(){
        return appPrefs;
    }

    public static Context getContext(){
        return context;
    }

}
