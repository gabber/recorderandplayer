package dgroup.apps.anecdots.core;


import android.database.Cursor;
import android.os.AsyncTask;

public final class SearchTask extends AsyncTask<Void, String, Boolean> {

    private Boolean mResult;
    private String mProgressMessage;
    private IProgressTracker mProgressTracker;
    private String textSearch;

    public SearchTask(String textSearch) {
        this.textSearch = textSearch;

    }


    public void setProgressTracker(IProgressTracker progressTracker) {

        mProgressTracker = progressTracker;

        if (mProgressTracker != null) {
            mProgressTracker.onProgress(mProgressMessage);
            if (mResult != null) {
                mProgressTracker.onComplete();
            }
        }
    }


    @Override
    protected void onCancelled() {

        mProgressTracker = null;
    }
    

    @Override
    protected void onProgressUpdate(String... values) {

        mProgressMessage = values[0];

        if (mProgressTracker != null) {
            mProgressTracker.onProgress(mProgressMessage);
        }
    }


    @Override
    protected void onPostExecute(Boolean result) {

        mResult = result;

        if (mProgressTracker != null) {
            mProgressTracker.onComplete();
        }

        mProgressTracker = null;
    }


    @Override
    protected Boolean doInBackground(Void... arg0) {
        Cursor cursor = DBHelper.instance().search(textSearch);
        if(cursor!=null && cursor.getCount() > 0){
            DBHelper.instance().setCursor(cursor);
            return true;
        }else{
            if(cursor!=null)
                cursor.close();
            return false;
        }
    }

}