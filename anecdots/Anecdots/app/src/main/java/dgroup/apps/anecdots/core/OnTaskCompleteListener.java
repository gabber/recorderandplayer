package dgroup.apps.anecdots.core;

public interface OnTaskCompleteListener {

    void onTaskComplete(SearchTask task);

    void onTaskComplete(PrepareTask task);
}