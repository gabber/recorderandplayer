package dgroup.apps.anecdots.core;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;
import android.util.SparseBooleanArray;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class DBHelper {

    private static DBHelper self;

    private SQLiteDatabase db;

    private static final int DB_VERSION = 3;
    private static final String DB_NAME = "appbase";

    public static final String TABLE_NAME = "anecdots";

    public static final String CONTENT = "content";
    public static final String ISFAVORITE = "isfavorite";
    public static final String CATEGORY = "category";
    public static final String NUMBER = "number";

    //небольшой костыль для передачи курсора из одного активити в другое
    private Cursor cursor;

    private DBHelper(Context context) {
        AnecdotsBase anecdotsBase = new AnecdotsBase(context);
        db = anecdotsBase.getWritableDatabase();
    }


    public static DBHelper instance(Context context) {
        Log.i("ololo","create dbfactory");
        if (self == null) {
            self = new DBHelper(context);
        }
        return self;
    }

    public static DBHelper instance()  throws NullPointerException{
        //may be null
        return self;
    }

    public Cursor getAllStory(String category){
        String query = "SELECT * FROM "+TABLE_NAME+" WHERE "+CATEGORY+" = ?";
        return db.rawQuery(query,new String[]{category});
    }

    public boolean isEmpty(){
        String query = "SELECT * FROM "+TABLE_NAME+" LIMIT 1";
        Cursor c = db.rawQuery(query,null);
        if(c!=null && c.getCount()>0){
            c.close();
            return false;
        }else{
            c.close();
            return true;
        }
    }

    public Cursor getFavorites(){
        String query = "SELECT * FROM "+TABLE_NAME+" WHERE "+ISFAVORITE+" = 1";
        return db.rawQuery(query,null);
    }

    public Cursor getRandom(){
        String query = "SELECT * FROM "+TABLE_NAME+" ORDER BY RANDOM() LIMIT 500";
        return db.rawQuery(query,null);
    }

    public Cursor getAllCategories(){
        String query ="SELECT DISTINCT "+ CATEGORY+" FROM "+TABLE_NAME;
        return db.rawQuery(query,null);
    }

    public Cursor search(String searchString) {
        String query = "select * from " + TABLE_NAME + " where " + CONTENT + " like '%" + searchString.replace("'", "''").toLowerCase() + "%'  LIMIT 500 ";
        return db.rawQuery(query, null);
    }


    public void updateFavorites(SparseBooleanArray changes, String category){
        db.beginTransaction();
        ContentValues cv = new ContentValues();
        for(int i=0; i<changes.size(); i++){
            cv.put(ISFAVORITE,changes.valueAt(i));
            db.update(TABLE_NAME, cv, NUMBER + " = " + changes.keyAt(i) + " AND " + CATEGORY + " = '" + category + "'", null);
        }
        cv.clear();
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public void updateFavorites(HashMap<Key,Boolean> map){
        db.beginTransaction();
        ContentValues cv = new ContentValues();
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry)it.next();
            cv.put(ISFAVORITE,(boolean)pairs.getValue());
            db.update(TABLE_NAME, cv, NUMBER + " = " + ((Key)pairs.getKey()).getNumber() + " AND " + CATEGORY + " = '" + ((Key)pairs.getKey()).getCategory() + "'", null);
        }
        cv.clear();
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public void removeFavorite(String category, int number){
        ContentValues cv = new ContentValues();
        cv.put(ISFAVORITE,false);
        db.update(TABLE_NAME,cv,NUMBER + " = "+number + " AND "+CATEGORY +" = '"+category+"'",null);
        cv.clear();
    }

    public SQLiteStatement getStatement(String query){
        return  db.compileStatement(query);
    }

    public void insert(String insert){
        db.execSQL(insert);
    }

    public void beginTransaction(){
        db.beginTransaction();
    }

    public void setTransactionSuccessful(){
        db.setTransactionSuccessful();
    }

    public void endTransaction(){
        db.endTransaction();
    }

    public Cursor getCursor() {
        return cursor;
    }

    public void setCursor(Cursor cursor) {
        this.cursor = cursor;
    }

    private class AnecdotsBase extends SQLiteOpenHelper {

        private static final String CREATE_TABLE = "create table " + TABLE_NAME
                + " ( _id INTEGER primary key autoincrement, "+CONTENT +" TEXT, "+ISFAVORITE+" INTEGER, " + CATEGORY
                + " TEXT , "+NUMBER+" INTEGER);";

        public AnecdotsBase(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.d("ololo", "--- onCreate database ---");
            db.execSQL(CREATE_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            onCreate(db);
        }
    }
}
