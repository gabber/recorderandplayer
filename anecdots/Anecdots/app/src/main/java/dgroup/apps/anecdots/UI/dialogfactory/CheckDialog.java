package dgroup.apps.anecdots.UI.dialogfactory;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import dgroup.apps.anecdots.R;
import dgroup.apps.anecdots.UI.ScreenSlidePagerActivity;


class CheckDialog extends Dialog implements
        android.view.View.OnClickListener {

    private Activity activity;

    private Button yes, no;

    public CheckDialog(Activity activity) {
        super(activity);
        this.activity = activity;

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.check_dialog);
        yes = (Button) findViewById(R.id.btn_yes);
        no = (Button) findViewById(R.id.btn_no);
        yes.setOnClickListener(this);
        no.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_yes:
                ((ScreenSlidePagerActivity)activity).prepare();
                dismiss();
                break;
            case R.id.btn_no:
                dismiss();
                break;
            default:
                break;
        }
    }
}