package dgroup.apps.anecdots.UI.dialogfactory;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.WindowManager;

public class DialogCreator {

    public static final int MODE_SEARCH = 1;

    public static final int MODE_PROGRESS = 2;

    public static final int MODE_CHECK = 3;

    public static final int MODE_MENU = 4;

    public static final int MODE_PREPARE = 5;

    private Activity activity;

    public DialogCreator(Activity activity){
        this.activity = activity;
    }

    public Dialog getDialog(int mode){
        Dialog dialog;
        switch (mode) {
            case 1:
                dialog = new SearchDialog(activity);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                return dialog;
            case 2:
                ProgressDialog progressDialog = new ProgressDialog(activity);
                progressDialog.setCancelable(true);
                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                return progressDialog;
            case 3:
                CheckDialog checkDialog = new CheckDialog(activity);
                checkDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                return checkDialog;
            case 4:
                MenuDialog menuDialog = new MenuDialog(activity);
                menuDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                return menuDialog;
            case 5:
                PrepareDialog prepareDialog = new PrepareDialog(activity);
                prepareDialog.setCancelable(false);
                prepareDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                return prepareDialog;
            default:
                return null;
        }


    }

}