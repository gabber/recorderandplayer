package dgroup.apps.anecdots.UI.dialogfactory;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import dgroup.apps.anecdots.R;
import dgroup.apps.anecdots.UI.MainActivity;


public class SearchDialog extends Dialog implements
        android.view.View.OnClickListener {

    private Activity activity;

    private Button yes, no;

    private EditText searchtext;

    private String text;

    public SearchDialog(Activity activity) {
        super(activity);
        this.activity = activity;

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.search_dialog);
        searchtext = (EditText) findViewById(R.id.searchtext);
        if (text != null) {
             searchtext.setText(text);
             searchtext.setSelection(text.length());
        }
        yes = (Button) findViewById(R.id.btn_yes);
        no = (Button) findViewById(R.id.btn_no);
        yes.setOnClickListener(this);
        no.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_yes:
                if (getText().length() != 0) {
                    ((MainActivity) activity).setupTask(getText());
                     dismiss();
                } else {
                    Toast.makeText(activity, "Введите текст поиска", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_no:
                dismiss();
                break;
            default:
                break;
        }
    }

    public String getText(){
        return searchtext.getText().toString();
    }

    public void setText(String s){
        text = s;
    }
}