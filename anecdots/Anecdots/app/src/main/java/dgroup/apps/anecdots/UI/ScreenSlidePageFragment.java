package dgroup.apps.anecdots.UI;

import android.app.Fragment;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import dgroup.apps.anecdots.AnecdotsApplication;
import dgroup.apps.anecdots.R;

public class ScreenSlidePageFragment extends Fragment {

    private String text = "Произошла ошибка";

    private int position;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(
                R.layout.fragment_screen_slide_page, container,false);
        setRetainInstance(true);

        Bundle bundle=getArguments();
        if(bundle!=null){
            text = bundle.getString("content");
            position = bundle.getInt("pos");
        }

        TextView tv = (TextView)view.findViewById(R.id.text);
        tv.setText(text);

        int fontSize;
        int fontpos = AnecdotsApplication.getSharedPreferences().getInt("selectedfontpos",-1);
        if(fontpos == -1){
            fontSize = 18;
        }else{
            switch (fontpos){
                case 0:
                    fontSize = 14;
                    break;
                case 1:
                    fontSize = 18;
                    break;
                case 2:
                    fontSize = 22;
                    break;
                default:
                    fontSize = 18;
            }
        }

        tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);

        view.setTag("pos"+position);

        return view;
    }
}