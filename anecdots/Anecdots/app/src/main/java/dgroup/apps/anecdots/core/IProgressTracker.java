package dgroup.apps.anecdots.core;

public interface IProgressTracker {

    void onProgress(String message);

    void onComplete();
}