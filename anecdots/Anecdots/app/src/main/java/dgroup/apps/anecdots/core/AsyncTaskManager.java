package dgroup.apps.anecdots.core;


import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.util.Log;

import dgroup.apps.anecdots.UI.MainActivity;
import dgroup.apps.anecdots.UI.dialogfactory.DialogCreator;


public final class AsyncTaskManager implements IProgressTracker, OnCancelListener {
    
    private final OnTaskCompleteListener mTaskCompleteListener;
    private Dialog mProgressDialog;
    private SearchTask mAsyncTask;
    private PrepareTask mPrepareTask;
    private DialogCreator dialogCreator;

    private boolean isSearchTask;

    public AsyncTaskManager(MainActivity activity, OnTaskCompleteListener taskCompleteListener,boolean isSearch) {

        dialogCreator = new DialogCreator(activity);
        mTaskCompleteListener = taskCompleteListener;
        if(isSearch){
            mProgressDialog = dialogCreator.getDialog(DialogCreator.MODE_PROGRESS);
            mProgressDialog.setOnCancelListener(this);
        }else{
            mProgressDialog = dialogCreator.getDialog(DialogCreator.MODE_PREPARE);
            mProgressDialog.setOnCancelListener(this);
        }

    }

    public void setupTask(SearchTask asyncTask) {

        mAsyncTask = asyncTask;

        mAsyncTask.setProgressTracker(this);

        mAsyncTask.execute();
    }

    public void setupTask(PrepareTask asyncTask) {

        mPrepareTask = asyncTask;

        mPrepareTask.setProgressTracker(this);

        mPrepareTask.execute();
    }

    @Override
    public void onProgress(String message) {

        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }

    }

    @Override
    public void onCancel(DialogInterface dialog) {
        Log.i("ololo","onCancel cancle");
        mAsyncTask.cancel(true);
        mTaskCompleteListener.onTaskComplete(mAsyncTask);
        mAsyncTask = null;
    }
    
    @Override
    public void onComplete() {
        Log.i("ololo","onComplete eeeee");
        mProgressDialog.dismiss();
        if(mAsyncTask!=null) {
            mTaskCompleteListener.onTaskComplete(mAsyncTask);
            mAsyncTask = null;
        }else{
            mTaskCompleteListener.onTaskComplete(mPrepareTask);
            mPrepareTask = null;
        }

    }

    public Object retainTask() {

        if (mAsyncTask != null) {
            mAsyncTask.setProgressTracker(null);
            return mAsyncTask;
        }
        if(mPrepareTask!=null){
            mPrepareTask.setProgressTracker(null);
            return mPrepareTask;
        }
        return null;
    }

    public void handleRetainedTask(Object instance) {

        if (instance instanceof SearchTask) {
            mAsyncTask = (SearchTask) instance;
            mAsyncTask.setProgressTracker(this);
        }
        if (instance instanceof PrepareTask) {
            mPrepareTask = (PrepareTask) instance;
            mPrepareTask.setProgressTracker(this);
        }
    }

}